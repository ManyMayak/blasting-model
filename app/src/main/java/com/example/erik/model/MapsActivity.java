package com.example.erik.model;

import android.graphics.Color;
import android.graphics.Point;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.support.v7.widget.Toolbar;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.Random;

public class MapsActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleMap.OnMarkerDragListener,  GoogleMap.OnMapClickListener, View.OnClickListener
        {

    private GoogleMap mMap;
    private Marker Smarker;
    private static final double TARGET_LATITUDE=54.7247;
    private static final double TARGET_LONGITUDE=55.9408;
    EditText radius, massa, countHum;
    Spinner vv, cover;
    private SoundPool sp;
    private int soundIdShot;
    TextView info;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);

        toolbar=(Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Modeling the terrorist act");

        sp = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);
        sp.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener(){
                                         public void onLoadComplete(SoundPool sp, int sid, int status) {
        }});
        soundIdShot = sp.load(this, R.raw.shot, 1);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        radius=(EditText) findViewById(R.id.radius);
        massa=(EditText) findViewById(R.id.massa);
        vv=(Spinner) findViewById(R.id.spinner2);
        cover=(Spinner) findViewById(R.id.spinner);
        info=(TextView)findViewById(R.id.info);
        countHum=(EditText) findViewById(R.id.countHuman);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id=item.getItemId();
        if(id==R.id.menu_boom){
            double lat = TARGET_LATITUDE, lon = TARGET_LONGITUDE;
            sp.play(soundIdShot, 1, 1, 0, 0, 1);
            CircleOptions circleOptions = new CircleOptions().center(new LatLng(Smarker.getPosition().latitude, Smarker.getPosition().longitude))
                    .radius(Integer.parseInt(radius.getText().toString())).strokeColor(Color.RED)
                    .strokeWidth(5);
            mMap.addCircle(circleOptions);
            double vesh = 0, mater = 0, R = 0;
            Random random = new Random();
            switch (vv.getSelectedItemPosition()) {
                case 0:
                    vesh = 1;
                    break;
                case 1:
                    vesh = 1.53;
                    break;
                case 2:
                    vesh = 1.3;
                    break;
                case 3:
                    vesh = 1.39;
                    break;
                case 4:
                    vesh = 0.99;
                    break;
                case 5:
                    vesh = 0.66;
                    break;
                case 6:
                    vesh = 0.39;
                    break;
                case 7:
                    vesh = 1.15;
                    break;
                default:
                    break;
            }
            switch (cover.getSelectedItemPosition()) {
                case 0:
                    mater = 1;
                    break;
                case 1:
                    mater = 0.95;
                    break;
                case 2:
                    mater = 0.6;
                    break;
                case 3:
                    mater = 0.8;
                    break;


                default:
                    break;
            }

            String val = radius.getText().toString();
            String val2 = massa.getText().toString();
            String val3=countHum.getText().toString();
            R = IzbitDavl.raschetDavl(Double.parseDouble(val), Double.parseDouble(val2), mater, vesh);
            int victim=Integer.parseInt(val3);
            int possible=random.nextInt(10);
            String word;
            if (possible==0 || victim<possible) {
                word="нет";
            }else word=String.valueOf(possible);
            if (R >= 100) {
                info.setText("Полное разрушение и "+ (int)Math.ceil(victim*0.75) +" погибло, " +
                        "пострадали "+(int)Math.ceil(random.nextInt(victim-(int)Math.ceil(victim*0.75))))  ;

            } else if (R >= 70) {
                info.setText("Тяжелые повреждения и "+ (int)Math.ceil(victim* 0.35) +" погибло, " +
                        "пострадали "+ (int)Math.ceil(random.nextInt(victim-(int)Math.ceil(victim*0.35))));

            } else if (R >= 28) {
                info.setText("Среднее разрушение и "+ (int)Math.ceil(victim* 0.05) +" погибло, " +
                        "пострадали "+(int)Math.ceil(random.nextInt(victim-(int)Math.ceil(victim*0.05))));
            } else if (R >= 14) {
                info.setText("Разрушение оконных проемов и " + (int)Math.ceil(victim* 0.05) +" погибло, " +
                        "пострадали "+(int)Math.ceil(random.nextInt(victim-(int)Math.ceil(victim*0.05))));
            } else if (R >= 2) {
                info.setText("Частичное разрушение остекленения, погибших нет, пострадавших "+word);
            } else info.setText("Взрыв бомбочки");

        }
        return super.onOptionsItemSelected(item);
    }

            /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        sp.play(soundIdShot,1,1,0,0,1);
        mMap = googleMap;

        LatLng sydney = new LatLng(TARGET_LATITUDE, TARGET_LONGITUDE);

        CameraPosition cameraPosition=new CameraPosition.Builder()
                .target(sydney).zoom(20).bearing(45)
                .tilt(20)
                .build();
        mMap.setOnMapClickListener(this);

        mMap.setOnMarkerDragListener(this);
        CameraUpdate cameraUpdate = CameraUpdateFactory.newCameraPosition(cameraPosition);
        mMap.animateCamera(cameraUpdate);
        double lat=TARGET_LATITUDE;
        double lng=TARGET_LONGITUDE;
        if(null != mMap){
            Smarker=
            mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(lat, lng))
                    .title("Mark").draggable(true)
                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.atomic)));





    }


    }

    //    @Overrid
    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        marker.getPosition();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Smarker.remove();
        Smarker=
                mMap.addMarker(new MarkerOptions()
                        .position(new LatLng(latLng.latitude, latLng.longitude))
                        .title("Mark").draggable(true)
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.atomic)));
    }


            @Override
            public void onClick(View v) {

            }
        }
